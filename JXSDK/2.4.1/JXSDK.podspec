
Pod::Spec.new do |s|
  s.name = 'JXSDK'
  s.version = '2.4.1'
  s.authors = 'jiaxin'
  s.homepage = 'https://bitbucket.org/livechatsdkteam/ioslivechatsdk'
  #s.source = {:path => 'JXSDK_Lite.framework'}
  s.source = {:git => "https://garyhahaha@bitbucket.org/livechatsdkteam/ioslivechatsdk.git", :tag => s.version.to_s}
  s.summary = 'JXSDK_Lite'
  s.license = { :type => 'Comercial', :text => 'TVB.com: Copyright 2017 All Rights Reserved.' }
  s.author = { 'Live Chat' => 'mobile-development@tvb.com' }
  s.platform = :ios
  s.ios.deployment_target = '8.0'
  s.vendored_frameworks = 'vendor/JXSDK/JXSDK_Lite.framework', 'vendor/JXSDK/JXUIKit.framework'
  s.public_header_files  = 'vendor/JXSDK/JXSDK_Lite.framework/Headers/*.h', 'vendor/JXSDK/JXSDK_Lite.framework/Headers/**/*.h', 'vendor/JXSDK/JXUIKit.framework/Headers/*.h'
  s.source_files = 'vendor/JXSDK/JXSDK_Lite.framework/Headers/*.h', 'vendor/JXSDK/JXSDK_Lite.framework/Headers/**/*.h', 'vendor/JXSDK/JXUIKit.framework/Headers/*.h'
  s.resource = 'vendor/JXSDK/JXUIKit.framework/JXUIResources.bundle'
  s.frameworks = 'SystemConfiguration'
  s.library = 'c++', 'xml2', 'sqlite3', 'resolv'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(inherited) "$(PODS_ROOT)/JXSDK/vendor/JXSDK/JXUIKit.framework/Headers" "${PODS_ROOT}/JXSDK/vendor/JXSDK/JXSDK_Lite.framework/Headers" "${PODS_ROOT}/JXSDK/vendor/JXSDK/JXSDK_Lite.framework/Headers/models" "${PODS_ROOT}/JXSDK/vendor/JXSDK/JXSDK_Lite.framework/Headers/delegates"' }
  s.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '-lObjC'}
end
